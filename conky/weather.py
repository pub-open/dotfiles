#! /usr/bin/python2

import requests
import json

key = open('/home/babak/Dropbox/weather_key.txt', 'r').readline().strip()

r = requests.get('http://ipinfo.io')
lat, lon = json.loads(r.text)['loc'].split(',')

r = requests.get('http://api.openweathermap.org/data/2.5/weather', 
                params={'lat': lat, 'lon': lon, 'units': 'imperial',
                        'APPID': key})

result = json.loads(r.text)

print '%s %s %.0fF' % (result['name'], 
                        result['weather'][0]['description'], 
                        result['main']['temp'])
