#!/bin/bash
############################
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
############################

dot=~/dotfiles  # dotfiles directory
files=".vimrc .zshrc"
configfiles="i3/config i3/config.i3status i3/monitor_util.sh conky/conky.conf conky/conky_i3_bar.sh conky/weather.py conky/weather.sh ranger/commands.py rofi/config termite/config zathura/zathurarc"    # list of files/folders to symlink in homedir
configdirs="i3 conky rofi termite ranger zathura"

for f in $files; do
	echo "Copying $dot/$f to ~"
	cp $dot/$f ~
done

for d in $configdirs; do
	echo "Creating ~/.config/$d"
	mkdir -p ~/.config/$d
done

for f in $configfiles; do
	echo "Copying $dot/$f to ~/.config/$f"
	cp $dot/$f ~/.config/$f
done
