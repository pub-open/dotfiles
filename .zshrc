# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
# End of lines configured by zsh-newuser-install


# The following lines were added by compinstall
zstyle :compinstall filename '/home/babak/.zshrc'
autoload -Uz compinit promptinit
compinit
promptinit
# End of lines added by compinstall


### Auto completion setup (use double TAB)
zstyle ':completion:*' menu select
setopt COMPLETE_ALIASES


### Freezing/unfreezing terminal fix
ttyctl -f


### Recent directories (use with $ dirs -v and $ cd -<NUM>)
DIRSTACKFILE="$HOME/.cache/zsh/dirs"
if [[ -f $DIRSTACKFILE  ]] && [[ $#dirstack -eq 0  ]]; then
	dirstack=( ${(f)"$(< $DIRSTACKFILE)"}  )
	[[ -d $dirstack[1]  ]] && cd $dirstack[1]
fi
chpwd() {
	print -l $PWD ${(u)dirstack} >$DIRSTACKFILE
}

DIRSTACKSIZE=20

setopt AUTO_PUSHD PUSHD_SILENT PUSHD_TO_HOME

# Remove duplicate entries
setopt PUSHD_IGNORE_DUPS

# This reverts the +/- operators.
setopt PUSHD_MINUS


### Persistent rehash of newly installed executables
zstyle ':completion:*' rehash true


### Fish-like syntax highlighting
#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


POWERLEVEL9K_MODE='awesome-fontconfig'
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_STATUS_VERBOSE=false
POWERLEVEL9K_TIME_FORMAT="%D{\uf017 %H:%M \uf073 %m.%d}"
POWERLEVEL9K_VI_INSERT_MODE_STRING=" im  "
POWERLEVEL9K_VI_COMMAND_MODE_STRING=" im  "
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="\n╭─"
POWERLEVEL9K_MULTILINE_SECOND_PROMPT_PREFIX="╰── "
POWERLEVEL9K_RAM_ELEMENTS=(ram_free)
POWERLEVEL9K_VI_MODE_INSERT_BACKGROUND="224"
POWERLEVEL9K_VI_MODE_NORMAL_FOREGROUND="black"
POWERLEVEL9K_VI_MODE_NORMAL_BACKGROUND="210"
POWERLEVEL9K_VI_MODE_INSERT_FOREGROUND="black"
POWERLEVEL9K_OS_ICON_BACKGROUND="white"
POWERLEVEL9K_OS_ICON_FOREGROUND="black"
POWERLEVEL9K_TIME_BACKGROUND="250"
POWERLEVEL9K_RAM_BACKGROUND="230"
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon dir rbenv vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status vi_mode ram time)

### Antigen
source ~/antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Load the theme.
antigen theme bhilburn/powerlevel9k powerlevel9k

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle command-not-found
antigen bundle colorize
antigen bundle history-substring-search
antigen bundle zsh-users/zsh-syntax-highlighting

# Tell antigen that you're done.
antigen apply


function zle-line-init {
	powerlevel9k_prepare_prompts
	if (( ${+terminfo[smkx]}  )); then
		printf '%s' ${terminfo[smkx]}
	fi
	zle reset-prompt
	zle -R

}

#function zle-line-finish {
	#powerlevel9k_prepare_prompts
	#if (( ${+terminfo[rmkx]}  )); then
		#printf '%s' ${terminfo[rmkx]}
	#fi
	#zle reset-prompt
	#zle -R

#}

function zle-keymap-select {
	powerlevel9k_prepare_prompts
	zle reset-prompt
	zle -R

}

zle -N zle-line-init
#zle -N ale-line-finish
zle -N zle-keymap-select

### vi mode
# bind UP and DOWN arrow keys
#zmodload zsh/terminfo
#bindkey "$terminfo[kcuu1]" history-substring-search-up
#bindkey "$terminfo[kcud1]" history-substring-search-down

# bind k and j for VI mode
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

bindkey -v
export KEYTIMEOUT=1

#function zle-line-init zle-keymap-select {
	#VIM_PROMPT="%{$fg_bold[yellow]%} [% NORMAL]%  %{$reset_color%}"
	#RPS1="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/} $EPS1"
	#zle reset-prompt
#}
#zle -N zle-line-init
#zle -N zle-keymap-select


alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
