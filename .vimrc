" vim-plug setup [start]
call plug#begin('~/.vim/plugged')

function! BuildYCM(info)
	if a:info.status == 'installed' || a:info.force
		!./install.py --clang-completer --system-libclang
	endif
endfunction
Plug 'Valloric/YouCompleteMe', { 'do': function('BuildYCM')  }
Plug 'scrooloose/nerdcommenter'
Plug 'jiangmiao/auto-pairs'
Plug 'flazz/vim-colorschemes'
"Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'easymotion/vim-easymotion'
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
"Plug 'nvie/vim-togglemouse'
"Plug 'elixir-lang/vim-elixir'
"Plug 'slashmili/alchemist.vim'

call plug#end()
" Vundle setup [end]

" YouCompleteMe [start]
set completeopt-=preview
let g:ycm_add_preview_to_completeopt=0
" YouCompleteMe [end]

" Airline [start]
" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'"

" Populate fonts
let g:airline_powerline_fonts = 1

let g:airline_theme='base16_monokai'

set laststatus=2
" Airline [end]

" easymotion [start]
let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Bi-directional find motion
" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
nmap s <Plug>(easymotion-s)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
" nmap s <Plug>(easymotion-s2)

" Turn on case insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
" easymotion [end]

" Vim [start]
let mapleader=","

set number
set nowrap
set tabstop=4
set backspace=indent,eol,start	" allow backspacing over everything in insert mode
"set softtabstop=4
set autoindent    " always set autoindenting on
set copyindent    " copy the previous indentation on autoindenting
set shiftwidth=4
set shiftround    " use multiple of shiftwidth when indenting with '<' and '>'"
"set showmatch     " set show matching parenthesis
set ignorecase    " ignore case when searching
set smartcase     " ignore case if search pattern is all lowercase, case-sensitive otherwise
set smarttab      " insert tabs on the start of a line according to shiftwidth, not tabstop
"set noexpandtab
"set hlsearch      " highlight search terms
set incsearch     " show search matches as you type
set wildignore=*.swp,*.bak,*.pyc,*.class	" ignore some file extensions when completing names by pressing Tab
set title         " change the terminal's title"
"set visualbell    " don't beep
"set noerrorbells  " don't beep
set showcmd

filetype plugin indent on   " Automatically detect file types.
autocmd filetype python set expandtab	" file type specific setting

" color scheme settings [start]

"set t_Co=256	" fixes putty
"colorscheme wombat256

" The "^[" is a single character. You enter it by pressing Ctrl+v and then ESC.
"set t_8f=[38;2;%lu;%lu;%lum
"set t_8b=[48;2;%lu;%lu;%lum

let g:gruvbox_italic=1

" Change the color scheme here.
colorscheme gruvbox
set background=dark

" Makes the background transparent. Leave these out if you're not using a transparent terminal.
" highlight Normal ctermbg=NONE guibg=NONE
" highlight NonText ctermbg=NONE guibg=NONE

" This is what sets vim to use 24-bit colors. It will also work for any version of neovim newer than 0.1.4.
set termguicolors
" color scheme settings [end]

syntax on

set colorcolumn=110
highlight ColorColumn ctermbg=darkgray

set list
set listchars=tab:>.,trail:-,extends:#,nbsp:.
autocmd filetype html,xml set listchars-=tab:>.		" disable list on html and xml

autocmd BufRead,BufNewFile *.tex set filetype=tex
autocmd FileType tex set wrap linebreak nolist spell

set pastetoggle=<F2>	" F2 enables paste mode when in insert mode, press again to diable after pasting

set mouse=a                 " Automatically enable mouse usage
set mousehide               " Hide the mouse cursor while typing

" Save cursor position
augroup resCur
	autocmd!
	autocmd BufReadPost * call setpos(".", getpos("'\""))
augroup END

" This allows buffers to be hidden if you've modified a buffer.
" This is almost a must if you wish to use buffers in this way.
set hidden
set wildmenu

"" To open a new empty buffer
nmap <leader>T :enew<cr>

" Move to the next buffer
nmap <leader>l :bnext<CR>

" Move to the previous buffer
nmap <leader>h :bprevious<CR>

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd #<CR>

" Show all open buffers and their status
nmap <leader>bl :ls<CR>"

nnoremap ; :

" Use Q for formatting the current paragraph (or selection)
" Just set your cursor inside a paragraph and press Q (or select a visual block and press Q).
vmap Q gq
nmap Q gqap

nnoremap j gj
nnoremap k gk

" Easy window navigation, for example, press C-h instead of C-w then h
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

nmap <silent> ,/ :nohlsearch<CR>	" clear highlighted search
" Vim [end]
