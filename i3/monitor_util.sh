#! /bin/bash

EXT=$(xrandr | grep connected | grep -v disconnected | cut -d' ' -f1 | grep HDMI)
if [ "$EXT" != "" ];then
	:
else
	EXT=$(xrandr | grep connected | grep -v disconnected | cut -d' ' -f1 | grep VGA)
fi

INT=$(xrandr | grep connected | grep -v disconnected | cut -d' ' -f1 | grep LVDS)
if [ "$INT" != "" ];then
	:
else
	INT=$(xrandr | grep connected | grep -v disconnected | cut -d' ' -f1 | grep eDP)
	if [ "$INT" == "" ];then
		INT=$(xrandr | grep connected | grep -v disconnected | cut -d' ' -f1 | grep DVI)
	fi
fi

if [ "$EXT" != "" ];then
	if [ $1 == "external" ];then
		xrandr --output $EXT --primary --auto --output $INT --off
	elif [ $1 == "extleft" ];then
		xrandr --output $EXT --primary --auto --output $INT --auto --left-of $EXT
	elif [ $1 == "extright" ];then
		xrandr --output $EXT --primary --auto --output $INT --auto --right-of $EXT
	elif [ $1 == "mirror" ];then
		xrandr --output $EXT --primary --auto --output $INT --auto --same-as $EXT
	fi
else
	xrandr --output $INT --primary
fi
